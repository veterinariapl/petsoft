package com.practica.proyecto.repository;

import com.practica.proyecto.model.Mascota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MascotaRepository extends JpaRepository<Mascota, Long> {

    List<Mascota> findAllByEstadoMascota(Boolean state);

}



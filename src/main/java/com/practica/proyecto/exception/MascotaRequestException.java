package com.practica.proyecto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;



@ResponseStatus(HttpStatus.NOT_FOUND)

public class MascotaRequestException extends RuntimeException{

    public MascotaRequestException (String message){
        super(message);
    }

    public MascotaRequestException (String message, Throwable cause){
        super(message, cause);
    }

}

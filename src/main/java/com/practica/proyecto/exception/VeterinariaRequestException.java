package com.practica.proyecto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class VeterinariaRequestException extends RuntimeException  {

    public VeterinariaRequestException(String message){
        super(message);
    }

    public VeterinariaRequestException(String message, Throwable cause){
        super(message, cause);
    }
}

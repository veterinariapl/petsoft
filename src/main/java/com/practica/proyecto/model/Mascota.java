package com.practica.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mascota")
public class Mascota {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_mascota")
    private Long idMascota;

    @Column(name = "nombre_mascota")
    private String nombreMascota;

    @Column(name = "especie_mascota")
    private String especieMascota;

    @Column(name = "edad_mascota")
    private String edadMascota;

    @Column(name = "raza_mascota")
    private String razaMascota;

    @Column(name = "sexo_mascota")
    private String sexoMascota;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "estado_mascota")
    private boolean estadoMascota ;


    public Mascota() {
        // Constructor
    }

    public Mascota(Mascota mascota) {
        this.idMascota = mascota.idMascota;
        this.nombreMascota = mascota.nombreMascota;
        this.especieMascota = mascota.especieMascota;
        this.edadMascota = mascota.edadMascota;
        this.razaMascota = mascota.razaMascota;
        this.sexoMascota = mascota.sexoMascota;
        this.observaciones = mascota.observaciones;
        this.estadoMascota = mascota.estadoMascota;
    }

    public Long getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(Long idMascota) {
        this.idMascota = idMascota;
    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public String getEspecieMascota() {
        return especieMascota;
    }

    public void setEspecieMascota(String especieMascota) {
        this.especieMascota = especieMascota;
    }

    public String getEdadMascota() {
        return edadMascota;
    }

    public void setEdadMascota(String edadMascota) {
        this.edadMascota = edadMascota;
    }

    public String getRazaMascota() {
        return razaMascota;
    }

    public void setRazaMascota(String razaMascota) {
        this.razaMascota = razaMascota;
    }

    public String getSexoMascota() {
        return sexoMascota;
    }

    public void setSexoMascota(String sexoMascota) {
        this.sexoMascota = sexoMascota;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }


    public boolean isEstadoMascota() {
        return estadoMascota;
    }

    public void setEstadoMascota(boolean estadoMascota) {
        this.estadoMascota = estadoMascota;
    }
}

package com.practica.proyecto.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sucursales")
public class Sucursal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sucursal")
    private Long idSuc;

    @Column(name = "direccion_sucursal")
    private String direccionSucursal;

    @Column(name = "telefono_sucursal")
    private String telefonoSucursal;

    @Column(name = "imagen_sucursal")
    private String imagenSucursal;

    @Column(name = "fk_admin_sucursal")
    private Long fkAdmin;


    public Sucursal(Long idSuc, String direccionSucursal, String telefonoSucursal, String imagenSucursal) {
        this.idSuc = idSuc;
        this.direccionSucursal = direccionSucursal;
        this.telefonoSucursal = telefonoSucursal;
        this.imagenSucursal = imagenSucursal;
        this.fkAdmin = null;
    }

    public Sucursal() {
    }

    public Long getIdSuc() {
        return idSuc;
    }

    public void setIdSuc(Long idSuc) {
        this.idSuc = idSuc;
    }

    public String getDireccion() {
        return direccionSucursal;
    }

    public void setDireccion(String direccion) {
        this.direccionSucursal = direccion;
    }

    public String getTelefono() {
        return telefonoSucursal;
    }

    public void setTelefono(String telefono) {
        this.telefonoSucursal = telefono;
    }

    public String getImagen() {
        return imagenSucursal;
    }

    public void setImagen(String imagen) {
        this.imagenSucursal = imagen;
    }

    public Long getFkAdmin() {
        return fkAdmin;
    }

    public void setFkAdmin(Long fkAdmin) {
        this.fkAdmin = fkAdmin;
    }


}

package com.practica.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cita")
public class Cita {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_cita")
    private Long idCita;

    @Column(name = "hora_programada")
    private String horaProgramada;

    @Column(name = "fecha_programada")
    private String fechaProgramada;


    public Cita(Long idCita, String horaProgramada, String fechaProgramada) {
        this.idCita = idCita;
        this.horaProgramada = horaProgramada;
        this.fechaProgramada = fechaProgramada;
    }

    public Cita() {
        // constructor
    }

    public Long getIdCita() {
        return idCita;
    }

    public void setIdCita(Long idCita) {
        this.idCita = idCita;
    }

    public String getHoraProgramada() {
        return horaProgramada;
    }

    public void setHoraProgramada(String horaProgramada) {
        this.horaProgramada = horaProgramada;
    }

    public String getFechaProgramada() {
        return fechaProgramada;
    }

    public void setFechaProgramada(String fechaProgramada) {
        this.fechaProgramada = fechaProgramada;
    }


}
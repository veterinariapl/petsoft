package com.practica.proyecto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "detalleHistoriaClinica")
public class DetalleHistoriaClinica {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name= "id_detalleHistoriaClinica")
    private Long idDetalle;

    public DetalleHistoriaClinica(Long idDetalle) {
        this.idDetalle = idDetalle;
    }

    public DetalleHistoriaClinica() {
        // constructor
    }

    public Long getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Long idDetalle) {
        this.idDetalle = idDetalle;
    }
}

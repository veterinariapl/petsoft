package com.practica.proyecto.service;

import com.practica.proyecto.model.Mascota;
import com.practica.proyecto.repository.MascotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MascotaService {

    private final MascotaRepository mascotaRepository;

    @Autowired
    public MascotaService(MascotaRepository mascotaRepository) {

        this.mascotaRepository = mascotaRepository;
    }

    public Optional<Mascota> findById(Long idMas) {
        return mascotaRepository.findById(idMas);
    }

    public Mascota saveMascota(Mascota mascota) {
        return mascotaRepository.save(mascota);
    }

    public void deleteMascota(Long idMascota) {
        mascotaRepository.deleteById(idMascota);
    }

    public Mascota updateMascota(Mascota mascota) {
        return mascotaRepository.save(mascota);
    }

    public List<Mascota> findAll() {
        return mascotaRepository.findAll();
    }

    public List<Mascota> findAllByEstadoMascota(Boolean state) {
        return mascotaRepository.findAllByEstadoMascota(state);
    }

}

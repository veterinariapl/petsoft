package com.practica.proyecto.service;

import com.practica.proyecto.model.Sucursal;
import com.practica.proyecto.model.Veterinaria;
import com.practica.proyecto.model.VeterinariaSucDTO;
import com.practica.proyecto.repository.SucursalRepository;
import com.practica.proyecto.repository.VeterinariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VeterinariaService {
    private final VeterinariaRepository veterinariaRepository;
    private final SucursalRepository sucursalRepository;


    @Autowired
    public VeterinariaService(VeterinariaRepository veterinariaRepository, SucursalRepository sucursalRepository) {
        this.veterinariaRepository = veterinariaRepository;
        this.sucursalRepository = sucursalRepository;
    }

    public Veterinaria saveVeterinary(Veterinaria veterinaria) {
        return veterinariaRepository.save(veterinaria);
    }

    public Veterinaria updateVeterinary(Veterinaria veterinaria) {
        return veterinariaRepository.save(veterinaria);
    }

    public Optional<Veterinaria> findById(Long id) {
        return veterinariaRepository.findById(id);
    }

    public List<Veterinaria> findAll() {
        return veterinariaRepository.findAll();
    }

    public Optional<VeterinariaSucDTO> findbyIdVetSuc(Long idVet, Long idSuc) {
        VeterinariaSucDTO vetDto = new VeterinariaSucDTO();
        Optional<Veterinaria> vet = veterinariaRepository.findById(idVet);
        Optional<Sucursal> suc = sucursalRepository.findById(idSuc);
        if (vet.isPresent() && suc.isPresent()) {
            vetDto.setNit(vet.get().getNit());
            vetDto.setNombreVet(vet.get().getNombre());
            vetDto.setCorreo(vet.get().getCorreo());
            vetDto.setDireccion(suc.get().getDireccion());
            vetDto.setTelefono(suc.get().getTelefono());
            vetDto.setImagen(suc.get().getImagen());

        }
        return Optional.of(vetDto);

    }

    public void deleteVeterinary(Long id) {
        veterinariaRepository.deleteById(id);
    }

    public List<Veterinaria> findByEstado(Boolean state) {
        return veterinariaRepository.findByEstado(state);
    }


}

package com.practica.proyecto.controller;

import com.practica.proyecto.exception.VeterinariaRequestException;
import com.practica.proyecto.model.Veterinaria;
import com.practica.proyecto.model.VeterinariaSucDTO;
import com.practica.proyecto.service.VeterinariaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/veterinary")
@Api("veterinary")
public class VeterinariaController {

    private final VeterinariaService veterinariaService;

    @Autowired
    public VeterinariaController(VeterinariaService veterinariaService) {
        this.veterinariaService = veterinariaService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "add veterinary", response = Veterinaria.class)
    public Veterinaria saveVeterinary(@RequestBody Veterinaria veterinaria) {
        return veterinariaService.saveVeterinary(veterinaria);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "delete  veterinary", response = Veterinaria.class)
    public void deleteVeterinary(@RequestParam(name = "id") Long id) {
        veterinariaService.deleteVeterinary(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update veterinary", response = Veterinaria.class)
    public Veterinaria updateVeterinary(@RequestBody Veterinaria veterinaria) {
        return veterinariaService.updateVeterinary(veterinaria);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "find all veterinaries", response = Veterinaria.class)
    public List<Veterinaria> findAll(){
        return veterinariaService.findAll();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "find veterinary by id", response = Veterinaria.class)
    public ResponseEntity<Veterinaria> findById(@RequestParam(name = "id") Long id) {
        VeterinariaRequestException veteException = new VeterinariaRequestException("No se encontro la veterinaria");
        return ResponseEntity.ok(veterinariaService.findById(id)
                .orElseThrow(() -> veteException));
    }

    @GetMapping(path = "/veterinary/info")
    @ApiOperation(value = "find by id veterinary and branch office", response = VeterinariaSucDTO.class)
    public ResponseEntity<VeterinariaSucDTO> findbyIdVetSuc(@RequestParam(name = "idvet") Long idVet, @RequestParam(name = "idsuc") Long idSuc) {
        VeterinariaRequestException veteException = new VeterinariaRequestException("No se pudo encontrar la información de la veterinaria con la sucursal");
        return ResponseEntity.ok(veterinariaService.findbyIdVetSuc(idVet, idSuc)
                .orElseThrow(() -> veteException));
    }

    @GetMapping(path = "/all/Active")
    @ApiOperation(value = "veterinaries active", response = Veterinaria.class)
    public List<Veterinaria> findByEstado(@RequestParam(name = "state") Boolean state) {
        return veterinariaService.findByEstado(state);
    }


}

package com.practica.proyecto.controller;

import com.practica.proyecto.exception.MascotaRequestException;
import com.practica.proyecto.model.Mascota;
import com.practica.proyecto.service.MascotaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/mascota")
@Api("mascota")
public class MascotaController {

    private final MascotaService mascotaService;

    @Autowired
    public MascotaController(MascotaService mascotaService) {
        this.mascotaService = mascotaService;
    }


    @PostMapping(path = "/save")
    @ApiOperation(value = "Save pets", response = Mascota.class)
    public Mascota saveMascota(@RequestBody Mascota mascota) {
        return mascotaService.saveMascota(mascota);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update pets", response = Mascota.class)
    public Mascota updateMascota(@RequestBody Mascota mascota) {
        return mascotaService.updateMascota(mascota);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get all pets", response = Mascota.class)
    public List<Mascota> findAll() {
        return mascotaService.findAll();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get user by id", response = Mascota.class)
    public ResponseEntity<Mascota> findById(@RequestParam(name = "id") Long id) {
        return ResponseEntity.ok(mascotaService.findById(id)
                .orElseThrow(() -> new MascotaRequestException("No se encontro una mascota")));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete user by id", response = Mascota.class)
    public void deleteMascota(@RequestParam(name = "id") Long id) {
        mascotaService.deleteMascota(id);
    }

    @GetMapping(path = "/all/state")
    @ApiOperation(value = "Get pets by state", response = Mascota.class)
    public List<Mascota> findAllByEstadoMascota(@RequestParam(name = "state") Boolean state) {
        return mascotaService.findAllByEstadoMascota(state);
    }

}






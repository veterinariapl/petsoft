package com.practica.proyecto.controller;

import com.practica.proyecto.model.Appointment;
import com.practica.proyecto.service.AppointmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/appointment")
@Api("Appointment")
public class AppointmentController {
    private final AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService){
        this.appointmentService = appointmentService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Crate an appointment", response = Appointment.class)
    public Appointment saveAppointment(@RequestBody Appointment appointment){
        return appointmentService.saveAppointment(appointment);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value ="See all appointments",response = Appointment.class)
    public List<Appointment> findAll(){
        return appointmentService.findAll();
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update an appointment data",response = Appointment.class)
    public Appointment updateAppointment(@RequestBody Appointment appointment){
        return appointmentService.updateAppointment(appointment);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete appointment by id",response = Appointment.class)
    public void deleteById(@RequestParam(name="appointmentId") Long appointmentId){
        appointmentService.deleteAppointment(appointmentId);
    }

}

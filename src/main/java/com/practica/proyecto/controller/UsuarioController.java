package com.practica.proyecto.controller;

import com.practica.proyecto.model.Usuario;
import com.practica.proyecto.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usuario")
@Api("Usuario")
public class UsuarioController {

    private final UsuarioService usuarioService;

    @Autowired
    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Insert Users", response = Usuario.class)
    public Usuario saveUsuario(@RequestBody Usuario usuario) {
        return usuarioService.saveUsuario(usuario);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Users", response = Usuario.class)
    public Usuario updateUsuario(@RequestBody Usuario usuario) {
        return usuarioService.updateUsuario(usuario);
    }

    @GetMapping(path = "/all/state")
    @ApiOperation(value = "Get users by state", response = Usuario.class)
    public List<Usuario> findAllByEstado(@RequestParam(name = "state") Boolean state) {
        return usuarioService.findAllByEstado(state);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Users", response = Usuario.class)
    public List<Usuario> findAll() {
        return usuarioService.findAll();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get user by id", response = Usuario.class)
    public Optional<Usuario> findById(@RequestParam(name = "numberId") Long numberId) {
        return usuarioService.findById(numberId);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete user by id", response = Usuario.class)
    public void deleteById(@RequestParam(name = "id") Long id) {
        usuarioService.deleteUser(id);
    }
}

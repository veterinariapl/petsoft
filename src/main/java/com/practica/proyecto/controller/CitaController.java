package com.practica.proyecto.controller;

import com.practica.proyecto.model.Cita;
import com.practica.proyecto.service.CitaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cita")
@Api("Cita")
public class CitaController {
    private final CitaService citaService;

    @Autowired
    public CitaController(CitaService citaService) {
        this.citaService = citaService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "agregue cita", response = Cita.class)
    public Cita saveCita(@RequestBody Cita cita) {
        return citaService.saveCita(cita);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "cargar todas las citas", response = Cita.class)
    public List<Cita> findAll() {
        return citaService.findAll();
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Date data", response = Cita.class)
    public Cita updateCita(@RequestBody Cita cita) {
        return citaService.updateCita(cita);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Date by id", response = Cita.class)
    public void deleteById(@RequestParam(name = "citaId") Long citaId) {
        citaService.deleteCita(citaId);
    }

}

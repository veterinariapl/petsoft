package com.practica.proyecto.controller;

import com.practica.proyecto.exception.VeterinariaRequestException;
import com.practica.proyecto.model.Veterinaria;
import com.practica.proyecto.model.VeterinariaDTO;
import com.practica.proyecto.service.VeterinariaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class VeterinariaControllerTest {

    private static final Long ID = Long.valueOf(1);
    private static final Long ID_SUC = Long.valueOf(1);
    private static final int ID_DUENIO = 1;
    private static final String NOMBRE = "patitas";
    private static final String NIT = "111111111";
    private static final String CORREO = "patitas@gmail.com";
    private static final Boolean ESTADO = true;
    private static final String TELEFONO = "4454341";
    private static final String IMAGEN = "foto.jpg";

    private static final Veterinaria VETERINARIA = new Veterinaria();
    private static final Optional<Veterinaria> OPTIONAL_VETERINARIA = Optional.of(VETERINARIA);

    private static final VeterinariaDTO VETERINARIA_DTO = new VeterinariaDTO();
    private static final Optional<VeterinariaDTO> OPTIONAL_VETERINARIA_DTO = Optional.of(VETERINARIA_DTO);

    private static final VeterinariaRequestException vetException = new VeterinariaRequestException("No se encontro la veterinaria para actualizar el estado");

    @Mock
    private VeterinariaService veterinariaService;

    @InjectMocks
    private VeterinariaController veterinariaController;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        VETERINARIA.setNombre(NOMBRE);
        VETERINARIA.setNit(NIT);
        VETERINARIA.setCorreo(CORREO);
        VETERINARIA.setEstado(ESTADO);
        VETERINARIA.setDuenio(ID_DUENIO);

        VETERINARIA_DTO.setNombreVet(NOMBRE);
        VETERINARIA_DTO.setNit(NIT);
        VETERINARIA_DTO.setCorreo(CORREO);
        VETERINARIA_DTO.setTelefono(TELEFONO);
        VETERINARIA_DTO.setImagen(IMAGEN);

    }

    @Test
    void saveVeterinaryTest() {
        Veterinaria vet;
        Mockito.when(veterinariaService.saveVeterinaria(VETERINARIA)).thenReturn(VETERINARIA);
        vet = veterinariaController.saveVeterinaria(VETERINARIA);
        assertEquals(VETERINARIA, vet);
        assertNotNull(vet);

    }

    @Test
    void findByIdTest() {
        Mockito.when(veterinariaService.findById(ID)).thenReturn(OPTIONAL_VETERINARIA);
        ResponseEntity<Veterinaria> vet = veterinariaController.findById(ID);
        assertNotNull(vet);
    }

    @Test
    void deleteVeterinariaTest() {

        Mockito.when(veterinariaService.findById(ID)).thenReturn(OPTIONAL_VETERINARIA);
        veterinariaController.deleteVeterinaria(ID);
    }

    @Test
    void findAllVeterinariansActivesTest() {
        final Veterinaria veterinaria = new Veterinaria();
        Mockito.when(veterinariaService.findAllVeterinariansActives()).thenReturn(Arrays.asList(veterinaria));
        final List<Veterinaria> resp = veterinariaController.findAllVeterinariansActives();
        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(1, resp.size());
    }

    @Test
    void findAllTest() {
        final Veterinaria veterinaria = new Veterinaria();
        Mockito.when(veterinariaService.findAll()).thenReturn(Arrays.asList(veterinaria));
        final List<Veterinaria> resp = veterinariaController.findAll();
        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(1, resp.size());
    }

    @Test
    void updateVeterinaryTest() {
        Mockito.when(veterinariaService.updateVeterinaria(VETERINARIA)).thenReturn(VETERINARIA);
        Veterinaria veterinaria = veterinariaController.updateVeterinaria(VETERINARIA);
        assertNotNull(veterinaria);
        assertEquals(VETERINARIA, veterinaria);

    }

    @Test
    void updateStateVetTest() {

        Mockito.when(veterinariaService.findById(ID)).thenReturn(OPTIONAL_VETERINARIA);
        Mockito.when(veterinariaService.updateStateVet(ID, true)).thenReturn(OPTIONAL_VETERINARIA);

        final ResponseEntity<Veterinaria> vet = veterinariaController.updateStateVet(ID, true);
        assertEquals(HttpStatus.OK, vet.getStatusCode());
    }

    @Test
    void findbyIdVetSucTest() {
        Mockito.when(veterinariaService.findbyIdVetSuc(ID, ID_SUC)).thenReturn(OPTIONAL_VETERINARIA_DTO);
        final ResponseEntity<VeterinariaDTO> resp = veterinariaController.findbyIdVetSuc(ID, ID_SUC);
        assertNotNull(resp);
        assertEquals(HttpStatus.OK, resp.getStatusCode());

    }


}

package com.practica.proyecto.controller;

import com.practica.proyecto.model.Sucursal;
import com.practica.proyecto.service.SucursalService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class SucursalControllerTest {

    private static final Long ID_SUCURSAL = Long.valueOf(1);
    private static final String DIRECCION = "calle busquela";
    private static final String TELEFONO_SUCU = "31454";
    private static final String IMAGEN_SUCU = "LOL.PNG";

    private static final Sucursal SUCURSAL = new Sucursal();
    private static final Optional<Sucursal> OPTIONAL_SUCURSAL = Optional.of(SUCURSAL);

    @Mock
    private SucursalService sucursalService;

    @InjectMocks
    private SucursalController sucursalController;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        SUCURSAL.setIdSuc(ID_SUCURSAL);
        SUCURSAL.setDireccion(DIRECCION);
        SUCURSAL.setTelefono(TELEFONO_SUCU);
        SUCURSAL.setImagen(IMAGEN_SUCU);


    }

    @Test
    void saveSucurtest() {
        Sucursal sucursal;
        Mockito.when(sucursalService.saveSucursal(SUCURSAL)).thenReturn(SUCURSAL);
        sucursal = sucursalController.saveSucursal(SUCURSAL);
        assertEquals(SUCURSAL, sucursal);
        assertNotNull(sucursal);
    }

    @Test
    void findAllTest() {
        final Sucursal sucursal = new Sucursal();
        Mockito.when(sucursalService.findAll()).thenReturn(Arrays.asList(sucursal));
        final List<Sucursal> resp = sucursalController.findAll();
        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(1, resp.size());
    }

    @Test
    void updateSucursalTest() {
        Mockito.when(sucursalService.updateSucursal(SUCURSAL)).thenReturn(SUCURSAL);
        Sucursal sucursal = sucursalController.updateSucursal(SUCURSAL);
        assertNotNull(sucursal);
        assertEquals(SUCURSAL, sucursal);

    }

    @Test
    void deleteSucursalTest() {

        Mockito.when(sucursalService.findById(ID_SUCURSAL)).thenReturn(OPTIONAL_SUCURSAL);
        sucursalController.deleteSucursal(ID_SUCURSAL);
    }

    @Test
    void findByIdTest() {
        Mockito.when(sucursalService.findById(ID_SUCURSAL)).thenReturn(OPTIONAL_SUCURSAL);
        Optional<Sucursal> suc = sucursalController.findById(ID_SUCURSAL);
        assertNotNull(suc);
    }
}

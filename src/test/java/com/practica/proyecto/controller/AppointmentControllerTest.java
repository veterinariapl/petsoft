package com.practica.proyecto.controller;

import com.practica.proyecto.model.Cita;
import com.practica.proyecto.service.CitaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class CitaControllerTest {
    private static final Long ID = Long.valueOf(1);
    private static final String HORA = "13:00";
    private static final String FECHA = "10/10/2020";

    private static final Cita CITA = new Cita();

    @Mock
    private CitaService citaService;

    @InjectMocks
    private CitaController citaController;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        CITA.setIdCita(ID);
        CITA.setHoraProgramada(HORA);
        CITA.setFechaProgramada(FECHA);

    }

    @Test
    void saveCitaTest(){
        Cita cita;
        Mockito.when(citaService.saveCita(CITA)).thenReturn(CITA);
        cita = citaController.saveCita(CITA);
        assertEquals(CITA, cita);
        assertNotNull(cita);
    }
}

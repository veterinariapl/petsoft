package com.practica.proyecto.controller;

import com.practica.proyecto.model.Mascota;
import com.practica.proyecto.service.MascotaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

public class MascotaControllerTest {

    private static final Long ID = Long.valueOf(2);
    private static final String NAME = "skylie2";
    private static final String SPECIE = "skylie2";
    private static final String AGE = "3";
    private static final String BREED = "criollo";
    private static final String SEX = "hembra";
    private static final String OBSERVATIONS = "blanco";
    private static final Boolean STATE = true;

    private  Mascota pet;
    private  Optional<Mascota> optionalPet;

    @Mock
    private MascotaService mascotaService;

    @InjectMocks
    MascotaController mascotaController;

    @BeforeEach
    public void init() {
        pet = mock(Mascota.class);
        optionalPet = Optional.of(pet);
        MockitoAnnotations.initMocks(this);
        pet.setNombreMascota(NAME);
        pet.setEspecieMascota(SPECIE);
        pet.setEdadMascota(AGE);
        pet.setRazaMascota(BREED);
        pet.setSexoMascota(SEX);
        pet.setObservaciones(OBSERVATIONS);
        pet.setEstadoMascota(STATE);
    }

    @Test
    void saveMascotaTest() {
        Mockito.when(mascotaService.saveMascota(pet)).thenReturn(pet);
        Mascota petSave = mascotaController.saveMascota(pet);

        assertEquals(pet, petSave);
        assertNotNull(petSave);
    }

    @Test
    void findByIdTest() {
        Mockito.when(mascotaService.findById(ID)).thenReturn(optionalPet);
        ResponseEntity<Mascota> masc = mascotaController.findById(ID);

        assertNotNull(masc);
    }

    @Test
    void deleteVeterinariaTest() {
        Mockito.when(mascotaService.findById(ID)).thenReturn(optionalPet);
        mascotaController.deleteMascota(ID);
    }

    @Test
    void findAllByEstadoMascota() {
        final Mascota mascota = new Mascota();
        Mockito.when(mascotaService.findAllByEstadoMascota(true)).thenReturn(Arrays.asList(mascota));
        final List<Mascota> resp = mascotaController.findAllByEstadoMascota(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }

    @Test
    void findAllTest() {
        final Mascota mascota = new Mascota();
        Mockito.when(mascotaService.findAll()).thenReturn(Arrays.asList(mascota));
        final List<Mascota> resp = mascotaController.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(1, resp.size());
    }

    @Test
    void updateMascotaTest() {
        Mockito.when(mascotaService.updateMascota(pet)).thenReturn(pet);
        Mascota petUpdate = mascotaController.updateMascota(pet);

        assertNotNull(petUpdate);
        assertEquals(pet, petUpdate);

    }

}

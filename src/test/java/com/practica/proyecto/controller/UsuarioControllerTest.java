package com.practica.proyecto.controller;

import com.practica.proyecto.model.Usuario;
import com.practica.proyecto.service.UsuarioService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class UsuarioControllerTest {

    private static final Long ID_USER = Long.valueOf(2);
    private static final String NAMES = "ARI";
    private static final String SURNAMES = "VALENCIA CEBALLOS";
    private static final String DOCUMENT_TYPE = "CC";
    private static final String DOCUMENT_VALUE = "1112496721";
    private static final Date BIRTH_DATE = new Date(1999,04,12);
    private static final String DEPARTMENT = "VAC";
    private static final String CITY = "CALI";
    private static final String NEIGHBORHOOD = "SILOE";
    private static final String TELEPHONE = "3163223023";
    private static final String EMAIL = "arivalencia@hotmail.com";
    private static final String PASSWORD = "Ari1234";
    private static final Boolean STATE = true;
    private static final String ROLE = "VET";
    private Usuario userController;
    private Optional<Usuario> optionalUserController;

    @Mock
    private UsuarioService usuarioService;

    @InjectMocks
    private UsuarioController usuarioController;

    @BeforeEach
    public void init(){
        userController = mock(Usuario.class);
        optionalUserController = Optional.of(userController);
        MockitoAnnotations.initMocks(this);
        userController.setId(ID_USER);
        userController.setNombre(NAMES);
        userController.setApellido(SURNAMES);
        userController.setTipoDocumento(DOCUMENT_TYPE);
        userController.setValorDocumento(DOCUMENT_VALUE);
        userController.setFechaNacimiento(BIRTH_DATE);
        userController.setDepartamento(DEPARTMENT);
        userController.setCiudad(CITY);
        userController.setBarrio(NEIGHBORHOOD);
        userController.setTelefono(TELEPHONE);
        userController.setCorreo(EMAIL);
        userController.setContrasenia(PASSWORD);
        userController.setEstado(STATE);
        userController.setRol(ROLE);
    }

    @Test
    void saveUsuarioTest(){
        Mockito.when(usuarioService.saveUsuario(userController)).thenReturn(userController);
        Usuario userSave = usuarioController.saveUsuario(userController);

        assertEquals(userController, userSave);
    }

    @Test
    void findByIdTest(){
        Mockito.when(usuarioService.findById(ID_USER)).thenReturn(optionalUserController);
        Optional<Usuario> user = usuarioController.findById(ID_USER);

        assertNotNull(user);
    }

    @Test
    void updateUsuarioTest(){
        Mockito.when(usuarioService.updateUsuario(userController)).thenReturn(userController);
        Usuario userUpdate = usuarioController.updateUsuario(userController);

        assertNotNull(userUpdate);
    }


    @Test
    void allActiveTest(){
        final Usuario user = new Usuario();
        Mockito.when(usuarioService.findAllByEstado(true)).thenReturn(Arrays.asList(user));
        final List<Usuario> resp = usuarioController.findAllByEstado(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
    }

    @Test
    void allTest(){
        final Usuario user = new Usuario();
        Mockito.when(usuarioService.findAll()).thenReturn(Arrays.asList(user));
        final List<Usuario> resp = usuarioController.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
    }

    @Test
    void deleteTest(){
        Mockito.when(usuarioService.findById(ID_USER)).thenReturn(optionalUserController);
        usuarioController.deleteById(ID_USER);
    }
}

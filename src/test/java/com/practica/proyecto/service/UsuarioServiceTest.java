package com.practica.proyecto.service;

import com.practica.proyecto.model.Usuario;
import com.practica.proyecto.repository.UsuarioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class UsuarioServiceTest {

    private static final Long ID_USER = Long.valueOf(1);
    private static final String NAMES = "GERMAN";
    private static final String SURNAMES = "DELGADO JIMENEZ";
    private static final String DOCUMENT_TYPE = "CC";
    private static final String DOCUMENT_VALUE = "1112496720";
    private static final Date BIRTH_DATE = new Date(1999, 04, 19);
    private static final String DEPARTMENT = "VAC";
    private static final String CITY = "JAMUNDI";
    private static final String NEIGHBORHOOD = "LIBERTADORES";
    private static final String TELEPHONE = "3163223713";
    private static final String EMAIL = "german-1-9@hotmail.com";
    private static final String PASSWORD = "German0419";
    private static final Boolean STATE = true;
    private static final String ROLE = "ADMIN";
    private Usuario user;
    private Optional<Usuario> optionalUsuario;

    @Mock
    private UsuarioRepository usuarioRepository;

    @InjectMocks
    UsuarioService usuarioService;

    @BeforeEach
    public void init() {
        user = mock(Usuario.class);
        optionalUsuario = Optional.of(user);
        MockitoAnnotations.initMocks(this);
        user.setId(ID_USER);
        user.setNombre(NAMES);
        user.setApellido(SURNAMES);
        user.setTipoDocumento(DOCUMENT_TYPE);
        user.setValorDocumento(DOCUMENT_VALUE);
        user.setFechaNacimiento(BIRTH_DATE);
        user.setDepartamento(DEPARTMENT);
        user.setCiudad(CITY);
        user.setBarrio(NEIGHBORHOOD);
        user.setTelefono(TELEPHONE);
        user.setCorreo(EMAIL);
        user.setContrasenia(PASSWORD);
        user.setEstado(STATE);
        user.setRol(ROLE);
    }

    @Test
    void findAllByEstadoTest() {
        final int num = 1;
        final Usuario usuario = new Usuario();
        Mockito.when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario));
        final List<Usuario> resp = usuarioService.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), num);
    }

    @Test
    void findAll() {
        final int num = 1;
        final Usuario usuario = new Usuario();
        Mockito.when(usuarioRepository.findAllByEstado(true)).thenReturn(Arrays.asList(usuario));
        final List<Usuario> resp = usuarioService.findAllByEstado(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), num);
    }

    @Test
    void deleteUserTest() {
        final int num = 1;
        Mockito.when(usuarioRepository.findById(ID_USER)).thenReturn(optionalUsuario);
        usuarioRepository.deleteById(ID_USER);
        Mockito.verify(usuarioRepository, Mockito.times(num)).deleteById(ID_USER);
    }

    @Test
    void saveUserTest() {
        Mockito.when(usuarioRepository.save(user)).thenReturn(user);
        Usuario userSave = usuarioService.saveUsuario(user);

        assertNotNull(userSave);
    }

    @Test
    void updateUserTest() {
        Mockito.when(usuarioRepository.save(user)).thenReturn(user);
        Usuario userSave = usuarioService.updateUsuario(user);

        assertNotNull(userSave);
        assertEquals(user, userSave);
    }

    @Test
    void findByIdTest() {
        Mockito.when(usuarioRepository.findById(ID_USER)).thenReturn(optionalUsuario);
        usuarioService.findById(ID_USER);
    }

    @Test
    void deleteUser() {
        Mockito.when(usuarioRepository.findById(ID_USER)).thenReturn(optionalUsuario);
        usuarioService.deleteUser(ID_USER);
    }


}

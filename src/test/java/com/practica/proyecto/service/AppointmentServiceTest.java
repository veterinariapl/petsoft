package com.practica.proyecto.service;

import com.practica.proyecto.model.Appointment;

import com.practica.proyecto.repository.AppointmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class AppointmentServiceTest {

    public final long ID = Long.valueOf(1);
    private static final String HORA = "13:00";
    private static final String FECHA = "10/10/2020";

    private static final Appointment APPOINTMENT = new Appointment();

    @Mock
    private AppointmentRepository appointmentRepository;

    @InjectMocks
    private AppointmentService appointmentService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        APPOINTMENT.setId(ID);
        APPOINTMENT.setHour(HORA);
        APPOINTMENT.setDate(FECHA);
    }

    @Test
    void saveCitaTest(){
        Mockito.when(appointmentRepository.save(APPOINTMENT)).thenReturn(APPOINTMENT);
        Appointment appointment = appointmentService.saveAppointment(APPOINTMENT);
        assertNotNull(appointment);

    }
}

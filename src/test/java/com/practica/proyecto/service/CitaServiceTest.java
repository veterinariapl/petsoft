package com.practica.proyecto.service;

import com.practica.proyecto.model.Cita;
import com.practica.proyecto.repository.CitaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CitaServiceTest {

    public final long ID = Long.valueOf(1);
    private static final String HORA = "13:00";
    private static final String FECHA = "10/10/2020";

    private static final Cita CITA = new Cita();
    private static final Optional<Cita> OPTIONAL_CITA = Optional.of(CITA);


    @Mock
    private CitaRepository citaRepository;

    @InjectMocks
    private CitaService citaService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        CITA.setIdCita(ID);
        CITA.setHoraProgramada(HORA);
        CITA.setFechaProgramada(FECHA);
    }

    @Test
    void saveCitaTest(){
        Mockito.when(citaRepository.save(CITA)).thenReturn(CITA);
        Cita cita = citaService.saveCita(CITA);
        assertNotNull(cita);

    }

    @Test
    void updateCitaTest() {
        Mockito.when(citaRepository.save(CITA)).thenReturn(CITA);
        Cita cita  = citaService.updateCita(CITA);
        assertNotNull(cita);
        assertEquals(CITA, cita);

    }

    @Test
    void findByAllTest () {
        final Cita cita = new Cita();
        Mockito.when(citaRepository.findAll()).thenReturn(Arrays.asList(cita));
        final List<Cita> resp = citaService.findAll();
        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }

    @Test
    void deleteSCitaTest() {
        Mockito.when(citaRepository.findById(ID)).thenReturn(OPTIONAL_CITA);
        citaService.deleteCita(ID);
    }

    @Test
    void findByIdTest() {
        Mockito.when(citaRepository.findById(ID)).thenReturn(OPTIONAL_CITA);
        citaService.findById(ID);
    }
}

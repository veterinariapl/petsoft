package com.practica.proyecto.service;

import com.practica.proyecto.model.Mascota;
import com.practica.proyecto.repository.MascotaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class MascotaServiceTest {

    private static final Long ID = Long.valueOf(1);
    private static final String NAME = "skylie";
    private static final String SPECIE = "skylie";
    private static final String AGE = "2";
    private static final String BREED = "chriollo";
    private static final String SEX = "macho";
    private static final String OBSERVATIONS = "negro";
    private static final Boolean STATE = true;

    private Mascota pet;
    private Optional<Mascota> optionalPet;

    @Mock
    private MascotaRepository mascotaRepository;

    @InjectMocks
    MascotaService mascotaService;

    @BeforeEach
    public void init() {
        pet = mock(Mascota.class);
        optionalPet = Optional.of(pet);
        MockitoAnnotations.initMocks(this);
        pet.setNombreMascota(NAME);
        pet.setEspecieMascota(SPECIE);
        pet.setEdadMascota(AGE);
        pet.setRazaMascota(BREED);
        pet.setSexoMascota(SEX);
        pet.setObservaciones(OBSERVATIONS);
        pet.setEstadoMascota(STATE);
    }

    @Test
    void findByIdTest() {
        Mockito.when(mascotaRepository.findById(ID)).thenReturn(optionalPet);
        mascotaService.findById(ID);
    }

    @Test
    void saveMascota() {
        Mockito.when(mascotaRepository.save(pet)).thenReturn(pet);
        Mascota petSave = mascotaService.saveMascota(pet);

        assertNotNull(petSave);
    }

    @Test
    void updateMascota() {
        Mockito.when(mascotaRepository.save(pet)).thenReturn(pet);
        Mascota petUpdate = mascotaService.updateMascota(pet);

        assertNotNull(petUpdate);
        assertEquals(pet, petUpdate);
    }

    @Test
    void findAllByEstadoMascotaTest() {
        final Mascota mascota = new Mascota();
        Mockito.when(mascotaRepository.findAllByEstadoMascota(true)).thenReturn(Arrays.asList(mascota));
        final List<Mascota> resp = mascotaService.findAllByEstadoMascota(true);

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(1, resp.size());
    }

    @Test
    void findByAllTest() {
        final Mascota mascota = new Mascota();
        Mockito.when(mascotaRepository.findAll()).thenReturn(Arrays.asList(mascota));
        final List<Mascota> resp = mascotaService.findAll();

        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }


    @Test
    void deleteMascotaTest() {
        Mockito.when(mascotaRepository.findById(ID)).thenReturn(optionalPet);
        mascotaService.deleteMascota(ID);
    }


}

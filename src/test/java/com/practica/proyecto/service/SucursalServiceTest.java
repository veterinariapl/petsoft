package com.practica.proyecto.service;

import com.practica.proyecto.model.Sucursal;
import com.practica.proyecto.repository.SucursalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class SucursalServiceTest {

    private static final Long ID_SUCURSAL = Long.valueOf(1);
    private static final String DIRECCION = "calle busquela con carrera encuentrela";
    private static final String TELEFONO_SUCU = "31454";
    private static final String IMAGEN_SUCU = "LOL.PNG";

    private static final Sucursal SUCURSAL = new Sucursal();
    private static final Optional<Sucursal> OPTIONAL_SUCURSAL = Optional.of(SUCURSAL);


    @Mock
    private SucursalRepository sucursalRepository;

    @InjectMocks
    private SucursalService sucursalService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        SUCURSAL.setIdSuc(ID_SUCURSAL);
        SUCURSAL.setDireccion(DIRECCION);
        SUCURSAL.setTelefono(TELEFONO_SUCU);
        SUCURSAL.setImagen(IMAGEN_SUCU);


    }

    @Test
    void saveSucurtest() {
        Mockito.when(sucursalRepository.save(SUCURSAL)).thenReturn(SUCURSAL);
        Sucursal sucursal = sucursalService.saveSucursal(SUCURSAL);
        assertNotNull(sucursal);
    }

    @Test
    void updateSucursalTest() {
        Mockito.when(sucursalRepository.save(SUCURSAL)).thenReturn(SUCURSAL);
        Sucursal sucursal = sucursalService.updateSucursal(SUCURSAL);
        assertNotNull(sucursal);
        assertEquals(SUCURSAL, sucursal);

    }

    @Test
    void findByAllTest() {
        final Sucursal sucursal = new Sucursal();
        Mockito.when(sucursalRepository.findAll()).thenReturn(Arrays.asList(sucursal));
        final List<Sucursal> resp = sucursalService.findAll();
        assertNotNull(resp);
        assertFalse(resp.isEmpty());
        assertEquals(resp.size(), 1);
    }

    @Test
    void deleteSucursaltest() {
        Mockito.when(sucursalRepository.findById(ID_SUCURSAL)).thenReturn(OPTIONAL_SUCURSAL);
        sucursalService.deleteSucursal(ID_SUCURSAL);
    }

    @Test
    void findByIdTest() {
        Mockito.when(sucursalRepository.findById(ID_SUCURSAL)).thenReturn(OPTIONAL_SUCURSAL);
        sucursalService.findById(ID_SUCURSAL);
    }

}
